/**
 * 
 */
package com.dbs.hackhire.dbshackhire.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Dinesh
 *
 */
@Entity
@Table(name="EQUITY")
public class Equity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Equity_Id")
	private long equityId;
	
	@Column(name="Level_Equity")
	private int levelEquity;
	
	@Column(name="Unique_Identifier")
	private String uniqueIdentifier;
	
	@Column(name="Equity_Name")
	private String equityName;
	
	@Column(name="Top_Level_Trade_Type")
	private String topLevelTradeType;
	
	@Column(name="Unique_Identifier_Top_Level")
	private String uniqueIdentifierTopLevel;
	
	@Column(name="Trade_Id")
	private int  tradeId;
	
	@Column(name="Current_Market_Value")
	private String currentMarketValue;

	
	
	public long getEquityId() {
		return equityId;
	}

	public void setEquityId(long equityId) {
		this.equityId = equityId;
	}

	public int getLevelEquity() {
		return levelEquity;
	}

	public void setLevelEquity(int levelEquity) {
		this.levelEquity = levelEquity;
	}

	public String getUniqueIdentifier() {
		return uniqueIdentifier;
	}

	public void setUniqueIdentifier(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}

	public String getEquityName() {
		return equityName;
	}

	public void setEquityName(String equityName) {
		this.equityName = equityName;
	}

	public String getTopLevelTradeType() {
		return topLevelTradeType;
	}

	public void setTopLevelTradeType(String topLevelTradeType) {
		this.topLevelTradeType = topLevelTradeType;
	}

	public String getUniqueIdentifierTopLevel() {
		return uniqueIdentifierTopLevel;
	}

	public void setUniqueIdentifierTopLevel(String uniqueIdentifierTopLevel) {
		this.uniqueIdentifierTopLevel = uniqueIdentifierTopLevel;
	}

	public int getTradeId() {
		return tradeId;
	}

	public void setTradeId(int tradeId) {
		this.tradeId = tradeId;
	}

	public String getCurrentMarketValue() {
		return currentMarketValue;
	}

	public void setCurrentMarketValue(String currentMarketValue) {
		this.currentMarketValue = currentMarketValue;
	}

	
	
	
	
	
	
	
}
