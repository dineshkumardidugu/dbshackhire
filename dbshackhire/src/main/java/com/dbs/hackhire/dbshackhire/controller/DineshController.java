/**
 * 
 */
package com.dbs.hackhire.dbshackhire.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.hackhire.dbshackhire.service.EquityService;

/**
 * @author Dinesh
 *
 */
@RestController
public class DineshController {
	
	@Autowired
	private EquityService equityService;
	
	@RequestMapping(value = "/report1" ,method = RequestMethod.GET)
	public void getReport1() {
		
		System.out.println("Inside");
		
		WritingExcelSheet();
		
		//equityService.getEquityInformation();
		
	}
	
	@RequestMapping(value="/report2",method = RequestMethod.GET)
	public void getReport2() {
		
		System.out.println("Inside report2");
	}
	
	
	
	public void WritingExcelSheet() {
		
		XSSFWorkbook workbook = new XSSFWorkbook(); 

		// Create a blank sheet 
		XSSFSheet sheet = workbook.createSheet("student Details"); 

		// This data needs to be written (Object[]) 
		Map<String, Object[]> data = new TreeMap<String, Object[]>(); 
		data.put("1", new Object[]{ "Level_Equity", "Unique_Identifier","Equity_Name","Top_Level_Trade_Type","Unique_Identifier_Top_Level","Trade_Id","Current_Market_Value" }); 
		data.put("2", new Object[]{ 3, "ABCD1234", "X1","FUTURES","FUTURES.IX",123456,"$350" }); 
		/*
		 * data.put("3", new Object[]{ 2, "Prakashni", "Yadav" }); data.put("4", new
		 * Object[]{ 3, "Ayan", "Mondal" }); data.put("5", new Object[]{ 4, "Virat",
		 * "kohli" });
		 */

		// Iterate over data and write to sheet 
		Set<String> keyset = data.keySet(); 
		int rownum = 0; 
		for (String key : keyset) { 
			// this creates a new row in the sheet 
			Row row = sheet.createRow(rownum++); 
			Object[] objArr = data.get(key); 
			int cellnum = 0; 
			for (Object obj : objArr) { 
				// this line creates a cell in the next column of that row 
				Cell cell = row.createCell(cellnum++); 
				if (obj instanceof String) 
					cell.setCellValue((String)obj); 
				else if (obj instanceof Integer) 
					cell.setCellValue((Integer)obj); 
			} 
		} 
		try { 
			// this Writes the workbook gfgcontribute 
			FileOutputStream out = new FileOutputStream(new File("D:\\gfgcontribute.xlsx")); 
			workbook.write(out); 
			out.close(); 
			System.out.println("gfgcontribute.xlsx written successfully on disk."); 
		} 
		catch (Exception e) { 
			e.printStackTrace(); 
		} 
		
	}

}
