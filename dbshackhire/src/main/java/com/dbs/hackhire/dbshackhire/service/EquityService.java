package com.dbs.hackhire.dbshackhire.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbs.hackhire.dbshackhire.model.Equity;
import com.dbs.hackhire.dbshackhire.repositary.EquityRepo;

@Service
public class EquityService {

	
	@Autowired
	EquityRepo  equityRepo;
	
	public List<Equity> getEquityInformation(){
		
		List<Equity> l1=new ArrayList<Equity>();
		
		l1=equityRepo.findAll();
		
		System.out.println("l1::"+l1);
		return l1;
		
	}
	
}
