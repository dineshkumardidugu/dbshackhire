package com.dbs.hackhire.dbshackhire.repositary;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dbs.hackhire.dbshackhire.model.Equity;

public interface EquityRepo extends JpaRepository<Equity,Long> {

}
