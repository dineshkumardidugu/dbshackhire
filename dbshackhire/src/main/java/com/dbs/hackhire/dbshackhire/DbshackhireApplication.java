package com.dbs.hackhire.dbshackhire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbshackhireApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbshackhireApplication.class, args);
	}

}
